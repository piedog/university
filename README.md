Lab Exercises – Classes & Objects (3)

WEEK 2 LABS 1

LAB CLASSES

STEP 1:
Create a new package (within JavaPrograms2) called Lab3 to store all the java
programs you create in this lab class.
STEP 2:
Create a new Java package called University (within Lab3).
EXERCISES
1.

Create three Class definitions Member.java, Staff.java and Student.java with attributes and
methods indicated in the diagram overleaf.
Note the following:
The Member class should be an abstract class with (i) a default constructor and (ii) another
constructor method to assign values to name and email (passed in as parameters).

The getStatus() method is an abstract method within the Member class. This method should
return the String “Staff” or “Student” from the appropriate sub-class.

Staff and Student are sub-classes of the Member class. Refer to the hierarchy diagram.

The Staff class should allocate a unique staff number (starting at 100001) to each member of
staff that is created.
(HINT: This will mean the use of a static int).

There are 3 constructors in the Staff class:
o

The default constructor method,

o

A constructor method that assigns values to ALL attributes (passed in as parameters) and

o

A constructor method that only assigns values to the name and email (passed in as
parameters) and assigns “” to job title and room, and 0 to salary.

The Student class should allocate a unique student number (start at 1) to each student created.
(HINT: This will mean the use of a static int).

There are 3 constructors in the Student class – the default one, one which assigns values to all
attributes (passed in as parameters) and one which only assigns values to the name, email and
course (passed in as parameters) and assigns -1 to each mark.

Software Development II (COM651)

1/6

Lab Exercises – Classes & Objects (3)

In addition to the set and get methods for the attributes shown in the diagram, the Student class
should have a method setMarks(…) which sets all 3 marks and a private method getAverage()
which returns the average of all three marks.

The toString() method in the Member class should return the name and email address in the
following format:
Name:

Niall McMullan

Email:

n.mcmullan@ulster.ac.uk

The toString() method in the Staff class should return all details of a member of staff in the
following format:
The Staff ID is 100001
Name:

Niall McMullan

Email:

n.mcmullan@ulster.ac.uk

Job Title: Lecturer
Room:

F34

Salary:

€60,000.00

Make use of the toString() method from the Member class (using super.toString()) to print out
the name and email. (HINT - using super.toString())

The toString() method in the Student class should return all details of a student in the following
format:
The Student ID is 1
Name:

Caitlin Johnston

Email:

johnston-c1@email.ulster.ac.uk

Course: Computing
The student's three marks are:
56%

67%

44%

The average mark of the student is 55.67%

Make use of the toString() method in Member to print out the name and email.
If no valid marks are recorded for a student (i.e. average returns -1), an appropriate message
should be output.

Define class methods numberOfStaff() and numberOfStudents() in the Staff and Student
classes respectively which return the number of Staff/Student objects which have been created.
Software Development II (COM651)

2/6

Lab Exercises – Classes & Objects (3)

Member
String name
String email
Member(String, String)
setName(String)
getName()

return String

setEmail(String)
getEmail()

return String

getStatus()

return String

toString()

return String

Staff

Student
int studentID

int staffID

String course

String jobTitle

int mark1

String room

int mark2

double salary

int mark3
Staff (String, String, String, String, double)
Student(String, String, String, int, int, int)

Staff (String, String)
getID()

return int

getID()

setJobTitle(String)
getJobTitle()

return String
return String

return String

setMark1(int)
getMark1()

setSalary(double)

return int

setCourse(String)
getCourse()

setRoom(String)
getRoom()

Student(String, String, String)

getSalary()

return double

setMark2(int)

getStatus()

return String

getMark2()

toString()

return String

setMark3(int)

numberOfStaff()

return int

getMark3()

return int

return int
return int

setMarks (int, int, int)

Software Development II (COM651)

getAverage()

return double

getStatus()

return String

toString()

return String

numberOfStudents()

return int
3/6

Lab Exercises – Classes & Objects (3)
1.1 Develop a test application class called TestMember.java.

TESTING STAFF
Provide code for the following:

Define a Staff object called staff1. This object has the following attributes:
Name: Niall McMullan
Email: n.mcmullan@ulster.ac.uk
Job Title: Lecturer
Room: F34
Salary: €60,000.00
Define a Staff object called staff2. This object has the following attributes:
Name: James Hollinger
Email: j.hollinger@ulster.ac.uk
The system should prompt the user to enter all 5 staff attributes, then use these to define a Staff
object called staff3.
Print out all the details of each member of staff.
Print out the number of staff objects created.
Set the salary of James Hollinger to €65,000.
Print out the details of staff2.
Print out the status of staff3.

TESTING STUDENT
Define a Student object called student1. This object has the following attributes:
Name: Caitlin Johnston
Email: johnston-c1@email.ulster.ac.uk
Course: Computing
Marks: 56, 67 and 44
Define a Student object called student2. This object has the following attributes:
Name: Jane Robinson
Email: robinson.j6@email.ulster.ac.uk
Course: Media Studies
Print out all the details of each student.
Print out the number of student objects created.

Software Development II (COM651)

4/6

Lab Exercises – Classes & Objects (3)
Provide code that will invoke the instance method called setMarks which will set the marks of
student2 to 80, 75 and 77.
Print out the details of student2.
Change mark2 of student1 to 76.
Print out the details of student1.

2.

Develop a Java Class called TimePiece to store information on timepieces such as stopwatches,
watches, clocks etc. Provide the class with the following instance variables (use your own
meaningful identifiers):

kind

e.g.

watch, stopwatch, clock

cost price

as a real number

readout

e.g.

analog, digital

hours

e.g.

0, 1, …22, 23

minutes

e.g.

0, 1, …58, 59

cantalk

as a true/false value (true means audio time is possible, false not)

2.1 Provide the class with the following 3 constructor methods. Ensure that any instance variables
whose values are not known are given suitable default values.
The (overridden) default constructor method
A constructor method for use whenever all information except for the current time is known
A constructor method for use whenever all of the details are passed as parameters.

2.2 Add to your class definition a method called showTime().
This method returns a String with the current time of the timepiece in the format
You should insert zeroes as appropriate

HH:MM

e.g. 03:05, 12:45

2.3 Add a toString() method which will return a String which outputs the details of the timepiece in the
following format
(NB Make use of the showTime() method):

Details of clock
==============================
Cost Price:
€2,500.00
Type of read out: analog
Current Time:
03:30
This time piece cannot talk
==============================

Software Development II (COM651)

5/6

Lab Exercises – Classes & Objects (3)
2.4 Add to your class definition an instance method called setMinutes(…). This method will take as a
parameter an integer value representing the time in minutes e.g. if the time is 12:45 then 45 should
be passed in.

2.5 Add to your class definition an instance method called setHours(…). This method will take as a
parameter an integer value representing the time in hours e.g. if the time is 12:45 then 12 should be
passed in.

2.6 Add to your class definition an instance method called setTime(…). This method will take as
parameters 2 integer values representing the time in hours and minutes e.g. if the time is 12:45 then
12 and 45 should be passed in. This method should call both the setMinutes(…) and setHours(…)
methods.

2.7 Add to your class definition an instance method called updateTime(…).This method will take as
parameters 2 integer values representing the time in hours and minutes to be added to the current
time e.g. if the time on our timepiece is slow by 1 hour 20 minutes, then 1 and 20 should be passed
in. If the current time is 12:45 and the timepiece is updated by 1:20, the new time will be 14:05.

2.8 Develop a test application class called TestTimePiece.java. Provide code for the following:
Define an object called myWatch. This object relates to a digital watch that cost €12.99. It can
speak and currently has the correct time of 9:45.

Define an object called bigBen. This object relates to an analog clock that cost €100,000.It
cannot speak.

Print out all the details of bigBen.

Use the instance method showTime() to print out the time of myWatch.

Use the instance method setMinutes(…) followed by a call to setHours(…) to set the time of
myWatch to 10:50.

Provide a single call to a method that would set the time on bigBen to 22:55.

Provide a suitable call to a method to change the time on bigBen by 2:05.

Print out all the details of myWatch and bigBen.

Software Development II (COM651)

6/6

